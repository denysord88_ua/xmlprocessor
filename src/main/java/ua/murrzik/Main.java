package ua.murrzik;

import ua.murrzik.dom.DOMCreater;
import ua.murrzik.dom.DOMParser;
import ua.murrzik.dom.DOMUpdater;
import ua.murrzik.jaxb.ConverterToObject;
import ua.murrzik.jaxb.ConverterToXML;
import ua.murrzik.sax.SAXParser;
import ua.murrzik.sax.SAXTrueExample;
import ua.murrzik.xpath.XPathParser;
import ua.murrzik.xpath.XPathQuery;

public class Main {
    public static void main (String [] args) {
        //DOMParser.parse();
        //DOMCreater.create();
        //DOMUpdater.update();
        //SAXParser.parse();
        //SAXTrueExample.parse();
        //XPathParser.parse();
        //XPathQuery.parse();
        //ConverterToXML.convertToXML();
        ConverterToObject.convertToObject();
    }
}