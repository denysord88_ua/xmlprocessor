package ua.murrzik.jaxb;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Created by DOrdynskiy on 18.11.2015.
 */
public class ConverterToObject {
    public static void convertToObject () {
        try {
            File file = new File("src/main/resources/xml/coursesJAXBConverterToXML.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Course.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Course course = (Course) jaxbUnmarshaller.unmarshal(file);
            System.out.println("Object - " + course);
            System.out.println("Object id - " + course.getId());
            System.out.println("Name : " + course.getName());
            System.out.println("Teacher : " + course.getTeacher());
            System.out.println("Hours : " + course.getHours());
            System.out.println("Day : " + course.getDay());
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
