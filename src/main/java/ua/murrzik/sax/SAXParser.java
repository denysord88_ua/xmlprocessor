package ua.murrzik.sax;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by DOrdynskiy on 18.11.2015.
 */
public class SAXParser {
    public static void parse () {
        SAXParserFactory factory = SAXParserFactory.newInstance();

        factory.setValidating(true);
        factory.setNamespaceAware(false);
        javax.xml.parsers.SAXParser parser;

        InputStream xmlData = null;
        try {
            xmlData = new FileInputStream("src/main/resources/xml/course.xml");

            parser = factory.newSAXParser();
            parser.parse(xmlData, new MyParser());
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }
}
