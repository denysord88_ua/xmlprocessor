package ua.murrzik.dom;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

/**
 * Created by DOrdynskiy on 18.11.2015.
 */
public class DOMCreater {
    public static void create() {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("progschool");
            doc.appendChild(rootElement);

            Element cource = doc.createElement("cource");
            rootElement.appendChild(cource);

            Attr attr = doc.createAttribute("id");
            attr.setValue("1");
            cource.setAttributeNode(attr);
            //cource.setAttribute("id", "1");

            Element name = doc.createElement("name");
            name.appendChild(doc.createTextNode("Java Level 1"));
            cource.appendChild(name);

            Element teacher = doc.createElement("teacher");
            teacher.appendChild(doc.createTextNode("Ivanov Ivan"));
            cource.appendChild(teacher);

            Element hours = doc.createElement("hours");
            hours.appendChild(doc.createTextNode("20"));
            cource.appendChild(hours);

            Element day = doc.createElement("day");
            day.appendChild(doc.createTextNode("friday"));
            cource.appendChild(day);

            // Преобразует DOM дерево в XML документ
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            // Создает DOM структуру из документа
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("src/main/resources/xml/coursesDOMCreator.xml"));

            // Преобразование DOM дерева в XML и запись его в поток result
            transformer.transform(source, result);

            System.out.println("\nFile saved!");
        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }
    }
}
