package ua.murrzik.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * Created by DOrdynskiy on 18.11.2015.
 */
public class DOMParser {
    public static void parse () {
        try {
            File fXMLFile = new File("src/main/resources/xml/course.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXMLFile);
            System.out.println("Root element : " + doc.getDocumentElement().getNodeName());
            NodeList nodeList = doc.getElementsByTagName("cource");
            for(int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                System.out.println("\nCurrent Element : " + node.getNodeName());
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    System.out.println("Cource id : " + element.getAttribute("id"));
                    System.out.println("Name : " + element.getElementsByTagName("name").item(0).getTextContent());
                    System.out.println("Teacher : " + element.getElementsByTagName("teacher").item(0).getTextContent());
                    System.out.println("Hours : " + element.getElementsByTagName("hours").item(0).getTextContent());
                    System.out.println("Day : " + element.getElementsByTagName("day").item(0).getTextContent());
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }
}
