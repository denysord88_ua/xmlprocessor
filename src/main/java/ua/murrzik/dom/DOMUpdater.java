package ua.murrzik.dom;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

/**
 * Created by DOrdynskiy on 18.11.2015.
 */
public class DOMUpdater {
    public static void update () {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse("src/main/resources/xml/course.xml");

            Node cource = doc.getElementsByTagName("cource").item(0);

            // ��������� ���� ���������� ���� cource
            NamedNodeMap attr = cource.getAttributes();
            Node nodeAttr = attr.getNamedItem("id");
            nodeAttr.setTextContent("25");

            Element category = doc.createElement("category");
            category.appendChild(doc.createTextNode("programming"));
            cource.appendChild(category);

            NodeList list = cource.getChildNodes();
            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);
                // update hours
                if ("hours".equals(node.getNodeName())) {
                    node.setTextContent("100");
                }
                // remove day
                if ("day".equals(node.getNodeName())) {
                    cource.removeChild(node);
                }
            }

            // ����������� DOM ������ � XML ��������
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            // ������� DOM ��������� �� ���������
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("src/main/resources/xml/coursesDOMUpdater.xml"));

            // �������������� DOM ������ � XML � ������ ��� � ����� result
            transformer.transform(source, result);

            System.out.println("\nFile updated!");
        } catch (ParserConfigurationException | IOException | SAXException | TransformerException e) {
            e.printStackTrace();
        }
    }
}
